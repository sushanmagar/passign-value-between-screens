import 'package:flutter/material.dart';
import 'package:pass_value_to_next_screen/Screens/screen2.dart';
import 'package:pass_value_to_next_screen/Screens/screen3.dart';

class ScreenOne extends StatelessWidget {
  String name;

  String age;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Screen One"),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.purple[100],
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 20),
              TextField(
                onChanged: (value) {
                  name = value;
                },
                decoration: InputDecoration(
                  hintText: "Enter your name",
                  labelText: "Name",
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 20),
              SizedBox(height: 20),
              TextField(
                onChanged: (value) {
                  age = value;
                },
                decoration: InputDecoration(
                  hintText: "Enter your age",
                  labelText: "Age",
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 20),
              Text(
                "Click the button below to pass value to stateless Screen",
                style: TextStyle(color: Colors.red),
              ),
              MaterialButton(
                color: Colors.red,
                height: 40,
                child: Text("Pass name"),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => ScreenTwo(
                        name: name,
                      ),
                    ),
                  );
                },
              ),
              SizedBox(height: 40),
              Text(
                "Click the button below to pass value to stateful Screen",
                style: TextStyle(color: Colors.red),
              ),
              MaterialButton(
                color: Colors.red,
                height: 40,
                child: Text("Pass age"),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (_) => ScreenThree(age: age)));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
