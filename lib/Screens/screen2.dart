import 'package:flutter/material.dart';

class ScreenTwo extends StatelessWidget {
  String name;

  ScreenTwo({this.name});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Screen Two (Stateless Example)"),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.purple[100],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Hello $name"),
            SizedBox(height: 20),
            MaterialButton(
              child: Text("Logout"),
              onPressed: () {
                Navigator.pop(context);
              },
              color: Colors.red,
            )
          ],
        ),
      ),
    );
  }
}
