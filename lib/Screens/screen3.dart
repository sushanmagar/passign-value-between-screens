import 'package:flutter/material.dart';

class ScreenThree extends StatefulWidget {
  String age;
  ScreenThree({Key key, @required this.age}) : super(key: key);
  @override
  _ScreenThreeState createState() => _ScreenThreeState(age: age);
}

class _ScreenThreeState extends State<ScreenThree> {
  String age;
  _ScreenThreeState({this.age});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Screen Two (Stateful Example)"),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.purple[100],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Your age: $age "),
            SizedBox(height: 20),
            MaterialButton(
              child: Text("Logout"),
              onPressed: () {
                Navigator.pop(context);
              },
              color: Colors.red,
            )
          ],
        ),
      ),
    );
  }
}
